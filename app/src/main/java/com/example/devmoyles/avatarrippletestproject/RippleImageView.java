package com.example.devmoyles.avatarrippletestproject;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by devmoyles on 08/11/2017.
 */

public class RippleImageView extends FrameLayout {

    private AnimationSet mSet1, mSet2;
    private ScaleAnimation mScaleAnim1, mScaleAnim2;
    private AlphaAnimation mAlphaAnim1, mAlphaAnim2;

    @BindView(R.id.iv_ripple_1)
    ClipRectImageView mIv1;
    @BindView(R.id.iv_ripple_2)
    ClipRectImageView mIv2;
    @BindView(R.id.iv_ripple_3)
    ImageView mIv3;
    @BindView(R.id.iv_avatar_ripple)
    RippleAvatarImageView mAvatarRipple;


    public RippleImageView(@NonNull Context context) {
        super(context);
        initView();
    }

    public RippleImageView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public RippleImageView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    public RippleImageView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView();
    }

    private void initView() {
        ButterKnife.bind(LayoutInflater.from(getContext())
                .inflate(R.layout.ripple_iv_layout, this, true));
    }

    public void setImage(int resource) {
//        mIv1.setImageResource(resource);
//        mIv2.setImageResource(resource);
//        mIv3.setImageResource(resource);
//        mAvatarRipple.setImageResource(resource);
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inMutable = true;
        mAvatarRipple.setAvatarBitmap(BitmapFactory.decodeResource(getResources(), resource, opts));

        adjustPadding();
    }

    public void setImage(Bitmap bitmap) {
        mIv1.setImageBitmap(bitmap);
        mIv2.setImageBitmap(bitmap);
        mIv3.setImageBitmap(bitmap);
        adjustPadding();
    }

    public void setImage(Drawable drawable) {
        mIv1.setImageDrawable(drawable);
        mIv2.setImageDrawable(drawable);
        mIv3.setImageDrawable(drawable);
        mIv2.setVisibility(INVISIBLE);
        mIv3.setVisibility(INVISIBLE);
        adjustPadding();
    }

    private void adjustPadding() {
        //to prevent annoying clipping that occurs.
        //I've tried clipChildren padding, outline to false. no dice.
        post(() -> {
//            int paddingAdjustment = (int) ((mIv1.getMeasuredHeight() * 1.3) - mIv1.getMeasuredHeight());
            int paddingAdjustment = (int) ((mAvatarRipple.getMeasuredHeight() * 1.3) - mAvatarRipple.getMeasuredHeight());
            setPadding(paddingAdjustment, paddingAdjustment, paddingAdjustment, paddingAdjustment);
            mIv1.updateRect();
        });
    }

    public void startRippleAnimation() {
        AnimationSet animation1 = (AnimationSet) AnimationUtils.loadAnimation(getContext(), R.anim.ripple_iv_anim);
        final AnimationSet animation2 = (AnimationSet) AnimationUtils.loadAnimation(getContext(), R.anim.ripple_iv_anim_no_repeat);
        animation1.getAnimations().get(0).setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Log.d("Animation %s", "end");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                mIv2.postDelayed(() -> mIv2.startAnimation(animation2), 600);
            }
        });

        mIv1.startAnimation(animation1);
    }
}
