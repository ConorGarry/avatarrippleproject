package com.example.devmoyles.avatarrippletestproject;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Region;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

/**
 * Created by conor on 09/11/2017.
 */

public class RippleAvatarImageView extends AppCompatImageView {

    private Bitmap mBitmap, mBitmap1;
    private Path mSmallPath = new Path(),
            mLargePath = new Path();
    private Paint mPaintAlpha = new Paint(Paint.ANTI_ALIAS_FLAG),
            mPaintFilter1 = new Paint(Paint.ANTI_ALIAS_FLAG),
            mPaintFilter2 = new Paint(Paint.ANTI_ALIAS_FLAG);
    private RectF mRectAnim1 = new RectF(),
            mRectAnim = new RectF();

    public RippleAvatarImageView(Context context) {
        super(context);
        init();
    }

    public RippleAvatarImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RippleAvatarImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setWillNotDraw(false);
        mPaintAlpha.setDither(true);
        mPaintAlpha.setAntiAlias(true);
        mPaintAlpha.setFilterBitmap(true);
        mPaintFilter1.setDither(true);
        mPaintFilter1.setAntiAlias(true);
        mPaintFilter1.setFilterBitmap(true);
        mPaintFilter2.setDither(true);
        mPaintFilter2.setAntiAlias(true);
        mPaintFilter2.setFilterBitmap(true);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (mBitmap == null) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        } else {
            setMeasuredDimension(mBitmap.getWidth(), mBitmap.getHeight());
        }
    }

    public void setAvatarBitmap(Bitmap b) {
        mBitmap = b.copy(b.getConfig(), true);
        int height = b.getWidth();
        int width = b.getHeight();
        int edgeVisible = (int)((width * 1.35) - width);

        //center co-ords for ImageView in both cases
        mLargePath.addCircle(width / 2,height / 2, width, Path.Direction.CCW);
        mSmallPath.addCircle(width / 2,height / 2, edgeVisible, Path.Direction.CCW);
        mBitmap1 = Bitmap.createBitmap(b.getWidth(), b.getHeight(), b.getConfig());
        // Clip unseen pixels by creating rings.
        Canvas outer = new Canvas(mBitmap1);
        outer.clipPath(mLargePath);
        outer.clipPath(mSmallPath, Region.Op.DIFFERENCE);
        outer.drawBitmap(b, 0, 0, mPaintAlpha);
        invalidate();

        // Scale animators
        ValueAnimator scale1 = (ValueAnimator) AnimatorInflater
                .loadAnimator(getContext(), R.animator.anim_call_scale_up);
        scale1.addUpdateListener(animation -> {
            mRectAnim.set(0, 0, (float) animation.getAnimatedValue(),
                    (float) animation.getAnimatedValue());
            mPaintFilter1.setAlpha((int) (100 - (float) animation.getAnimatedValue()));
            invalidate();
        });
        ValueAnimator scale2 = (ValueAnimator) AnimatorInflater
                .loadAnimator(getContext(), R.animator.anim_call_scale_up);
        scale2.addUpdateListener(animation -> {
            mRectAnim1.set(0, 0, ((float) animation.getAnimatedValue()),
                    (float) animation.getAnimatedValue());
            invalidate();
        });
        scale2.setStartDelay(600);

        // Alpha Animators
        ValueAnimator alpha1 = (ValueAnimator) AnimatorInflater
                .loadAnimator(getContext(), R.animator.alpha_disappear);
        alpha1.addUpdateListener(animation -> mPaintFilter1.setAlpha((Integer) animation.getAnimatedValue()));
        ValueAnimator alpha2 = (ValueAnimator) AnimatorInflater
                .loadAnimator(getContext(), R.animator.alpha_disappear);
        alpha2.addUpdateListener(animation -> mPaintFilter2.setAlpha((Integer) animation.getAnimatedValue()));
        alpha2.setStartDelay(600);
        // Combine animations and play as set.
        AnimatorSet set = new AnimatorSet();
        set.playTogether(scale1, scale2, alpha1, alpha2);
        set.start();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // Scale outer rings.
        canvas.save();
        canvas.scale(mRectAnim.width(), mRectAnim.height(), getWidth() / 2, getHeight() / 2);
        canvas.drawBitmap(mBitmap1, 0, 0, mPaintFilter1);
        canvas.restore();
        canvas.save();
        canvas.scale(mRectAnim1.width(), mRectAnim1.height(), getWidth() / 2, getHeight() / 2);
        canvas.drawBitmap(mBitmap1, 0, 0, mPaintFilter2);
        canvas.restore();

        // Main static image.
        canvas.drawBitmap(mBitmap, 0, 0, null);
    }
}
