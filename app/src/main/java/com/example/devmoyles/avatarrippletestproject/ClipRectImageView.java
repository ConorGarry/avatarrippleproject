package com.example.devmoyles.avatarrippletestproject;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Region;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ViewOutlineProvider;

/**
 * Created by devmoyles on 09/11/2017.
 */

public class ClipRectImageView extends android.support.v7.widget.AppCompatImageView {

    private Path mSmallPath, mLargePath;
    private ViewOutlineProvider mProvider;

    public ClipRectImageView(Context context) {
        super(context);
        initView();
    }

    public ClipRectImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public ClipRectImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }


    private void initView(){
        mLargePath = new Path();
        mSmallPath = new Path();
        updateRect();
    }

    public void updateRect(){
        post(() -> {
            Log.d("h&w", String.format("h %d, w %d", getMeasuredHeight(), getMeasuredWidth()));
            int width = getMeasuredWidth();
            int height = getMeasuredHeight();
            int edgeVisible = (int)((width * 1.4) - width);


            //center co-ords for imageview in both cases
            mLargePath.addCircle(width / 2,height / 2, width, Path.Direction.CCW);
            mSmallPath.addCircle(width / 2,height / 2, edgeVisible, Path.Direction.CCW);
            requestLayout();
        });
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.clipPath(mLargePath); // c is a Canvas
        canvas.clipPath(mSmallPath, Region.Op.DIFFERENCE);
        super.onDraw(canvas);
    }
}
